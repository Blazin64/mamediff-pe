"""
    MAMEDiff - Python Edition
    Copyright (C) 2021  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from lxml import etree as ET
from copy import copy

def xml_dict(machine, verbose):
    romset = {}
    name = machine.get("name")
    if verbose:
        print(f"Machine: {name}")
    romset[name] = {}
    data = romset[name]["roms"] = []
    roms = machine.findall("rom")
    for rom in roms:
        if verbose:
            print(f"    {rom.attrib}")
        data.append(copy(rom.attrib))
    dscr = machine.find("description")
    year = machine.find("year")
    manf = machine.find("manufacturer")
    romset[name]["dscr"] = ""
    romset[name]["year"] = ""
    romset[name]["manf"] = ""
    if not dscr == None:
        romset[name]["dscr"] = copy(dscr.text)
    if not year == None:
        romset[name]["year"] = copy(year.text)
    if not manf == None:
        romset[name]["manf"] = copy(manf.text)
    return romset


def read(filename, verbose, tags=["machine", "game"]):
    romset = {}
    for event, element in ET.iterparse(filename, tag=tags):
        romset.update(xml_dict(element, verbose))
        element.clear()
        del element
    return romset


def write(target, filename):
    head_name = "MAME Update"
    head_dscr = "MAME Update"
    head_vers = "MAME Update"
    head_auth = "MAMEDiff - Python Edition"
    encstr = "utf-8"
    docstr = '<!DOCTYPE datafile PUBLIC "-//Logiqx//DTD ROM Management Datafile//EN" "http://www.logiqx.com/Dats/datafile.dtd">'
    root = ET.Element("datafile")
    header = ET.SubElement(root, "header")
    ET.SubElement(header, "name").text = head_name
    ET.SubElement(header, "description").text = head_dscr
    ET.SubElement(header, "version").text = head_vers
    ET.SubElement(header, "author").text = head_auth
    for machine in target:
        temp = ET.SubElement(root, "machine", name=machine)
        ET.SubElement(temp, "description").text = target[machine]["dscr"]
        ET.SubElement(temp, "year").text = target[machine]["year"]
        ET.SubElement(temp, "manufacturer").text = target[machine]["manf"]
        for rom in target[machine]["roms"]:
            if "sha1" in rom:
                ET.SubElement(
                    temp,
                    "rom",
                    name=rom["name"],
                    size=rom["size"],
                    crc=rom["crc"],
                    sha1=rom["sha1"],
                )
    tree = ET.ElementTree(root)
    tree.write(
        filename,
        doctype=docstr,
        pretty_print=True,
        xml_declaration=True,
        encoding=encstr,
    )
