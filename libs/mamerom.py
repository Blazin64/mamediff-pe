"""
    MAMEDiff - Python Edition
    Copyright (C) 2021  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from collections import defaultdict


def map(romlist):
    hashes = {}
    for machine in romlist:
        for rom in romlist[machine]["roms"]:
            if "sha1" in rom:
                checksum = rom["sha1"]
                if not checksum in hashes:
                    hashes[checksum] = []
                hashes[checksum].append(machine)
    return hashes


def reverse_map(hashmap):
    revmap = defaultdict(list)
    for key, value in hashmap.items():
        for item in value:
            if not key in revmap[item]:
                revmap[item].append(key)
    return revmap


def map_changes(hashmap, romset_target):
    result = {}
    for machine, hashes in reverse_map(hashmap).items():
        target = romset_target[machine]
        result[machine] = {}
        result[machine]["dscr"] = target["dscr"]
        result[machine]["year"] = target["year"]
        result[machine]["manf"] = target["manf"]
        result[machine]["roms"] = []
        for checksum in hashes:
            for rom in target["roms"]:
                if "sha1" in rom:
                    if checksum == rom["sha1"]:
                        result[machine]["roms"].append(rom)
    return result


def changes(base, target):
    diffkeys = list(set(target.keys()).difference(base.keys()))
    diff = {}
    for key in diffkeys:
        diff[key] = target[key]
    change = []
    for rom in diff:
        machines = target[rom]
        for machine in machines:
            if not machine in change:
                change.append(machine)
    return diff, change
