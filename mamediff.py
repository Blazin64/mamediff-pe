#!/usr/bin/env python3

"""
    MAMEDiff - Python Edition
    Copyright (C) 2021  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import click
from libs import mamedat
from libs import mamerom


@click.command()
@click.option("-v", "--verbose", is_flag=True)
@click.option("--stats", is_flag=True)
@click.option("--base", required=True, type=click.Path(exists=True), prompt="Base DAT")
@click.option(
    "--target", required=True, type=click.Path(exists=True), prompt="Target DAT"
)
@click.option("--output", required=True, prompt="Output file name")
def action(base, target, stats, output, verbose):
    print(f'Base: Reading data from "{base}"\n')
    romset_base = mamedat.read(base, verbose)
    print("Base: Mapping checksums\n")
    map_base = mamerom.map(romset_base)
    print(f'Target: Reading data from "{target}"\n')
    romset_target = mamedat.read(target, verbose)
    print("Target: Mapping checksums\n")
    map_target = mamerom.map(romset_target)
    print("Detecting changes\n")
    diff, changed = mamerom.changes(map_base, map_target)
    print("Processing changes\n")
    result = mamerom.map_changes(diff, romset_target)

    if stats:
        print("Results")
        print(f"    New ROMs: {len(diff)}")
        print(f"    Affected ROM sets: {len(changed)}\n")

    print(f'Writing changes to "{output}"\n')
    mamedat.write(result, output)


if __name__ == "__main__":
    action()
