## Abandoned! Use [MAMEDiff - Rust Edition](https://gitlab.com/Blazin64/mamediff-rs) instead.

Even after extensive performance and memory usage optimizations, the Python version simply does not perform well. The Rust version is ~4.5x faster and uses ~3.5x less memory!

---

# MAMEDiff - Python Editon

![License GPLv3](https://badgen.net/badge/License/GPLv3/blue) ![Built With Python 3](https://badgen.net/badge/Built%20With/Python%203/yellow)

A CLI utility that helps create [MAME](https://www.mamedev.org/) update & rollback DAT files. It will eventually have the main features of Logiqx's [MAMEDiff](http://www.logiqx.com/Tools/).

Update DAT files can help determine exactly which files must be added for compatibility with a newer version of MAME. Rollback DAT files do the same thing, but for downgrading MAME versions instead.

You will need a ROM manager to use the processed DAT files. I've listed some under "Resources" at the end of the page.

__Minimum Required MAME Versions:__
* MAME 2003 (0.78)\*
* AdvanceMAME 0.94
* MAME 2010 (0.139)

\*Most DAT files for this version are in an old, unsupported format. However, [Progetto Snaps](https://www.progettosnaps.net/dats/MAME/) does have a DAT file that is supported. 😀

## Development Status:
__Core Features:__
| Feature                            | Status                    |
|------------------------------------|---------------------------|
| Read DAT/XML files                 | In progress (nearly done) |
| Write DAT/XML files                | In progress (nearly done) |
| Statistics reports                 | In progress (early stage) |
| Detailed reports                   | Not started               |
| Type Filters (CHDs, ROMs, Samples) | Not started               |

__Extra Features:__
TBD

## Technical Information

__Supported Formats:__
* MAME DAT
  * Used by MAME versions > 0.84
* MAME XML
  * Used by MAME versions > 0.68

__Unsupported Formats:__
* MAME DAT (listinfo format)
  * Used by MAME versions < 0.84
* MAME XML (without SHA1)
  * Used by MAME versions < 0.68

## Resources:

__Similar Tools__

| Name                                                   | Windows Support | Linux Support      | MacOS Support               |
|--------------------------------------------------------|-----------------|--------------------|-----------------------------|
| [MAMEDiff](http://www.logiqx.com/Tools/MAMEDiff/)      | Yes             | Yes, via WINE      | Yes, via WINE               |
| [SabreTools](https://github.com/SabreTools/SabreTools) | Yes             | No, broken in WINE | No, broken in WINE          |


__ROM Managers__

| Name                                                   | Windows Support | Linux Support | MacOS Support               |
|--------------------------------------------------------|-----------------|---------------|-----------------------------|
| [Clrmamepro](https://mamedev.emulab.it/clrmamepro/)    | Yes             | Yes, via WINE | Yes, via WINE               |
| [JRomManager](https://github.com/optyfr/JRomManager)   | Yes             | Yes           | Yes                         |
| [ROMba](https://github.com/uwedeportivo/romba/)        | No              | Yes           | Yes                         |
| [Romorganizer](https://github.com/xprism1/romog)       | No              | Yes           | No                          |
| [Romulus](https://github.com/ArthurMoore85/romulus)    | No              | Yes           | No                          |
| [ROMVault](https://romvault.com/)                      | Yes             | Yes, via Mono | Yes, via Mono               |
